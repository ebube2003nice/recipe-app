import React,{useEffect,useState} from 'react';
import './App.css';
import  Recipe from './Recipes';
const App=()=> {
const APP_ID = 'dedf557e';
const APP_KEY ="3d256c29668d9c3f0eb3628f14e7f77e";
const [recipes, setRecipes] = useState([]);

// For The Search Input
const[search, setSearch] = useState('');

const[query,setQuery] = useState("chicken")

useEffect( async ()=>{
getRecipes();
},[query]);

const getRecipes = async () =>{
  const response = await fetch(`https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`)
const data =await  response.json();
setRecipes(data.hits)
console.log(data.hits)

}

// Function to the search work
const updateSearch = e =>{
  setSearch(e.target.value)
}
const getSearch= e =>{
  e.preventDefault();
  setQuery(search);
  setSearch('');
}

  return (
    <div className="App">
<form onSubmit={getSearch} className="search-form">
  <input className="search-bar" type="text"  value={search} onChange={updateSearch}/>
  <button  className="search-button" type="submit">Search</button>
</form>
<div className="recipes">
{recipes.map(recipe =>(
  <Recipe 
  key={recipe.recipe.label}
   title={recipe.recipe.label} 
   calories={recipe.recipe.calories} 
  image={recipe.recipe.image}
  ingredients={recipe.recipe.ingredients}
  
  />
))}
</div>
    </div>
  );
}

export default App;
